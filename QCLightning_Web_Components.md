# Introduction to Lightning Web Components(LWC)

## What are lightning web components?

    Lightning Web Components (LWC) is an open-source, modern, and lightweight UI framework built on the latest W3C standards that uses native HTML and JavaScript to build Reactive SPA.

## What are the benefits of LWC?

    LWC provides improved speed and efficiency, better API support for third-party APIs, it's light-weight. It also has better security, testing capabilities, and browser compatibility.

## What are modules?

    Reusable units of Javascript code, you must place the export keyword infront of any items you wish to use inside another Javascript file, and use the import keyword followed by a comma-seperated list of items you wish to import from the module 
	Import example: import { functionName1, functionName2 } from './fileName';
	Export example: export { functionName1, functionName2 } or export className { }

## What is the Shadow DOM

    An important aspect of web components is encapsulation — being able to keep the markup structure, style, and behavior hidden and separate from other code, this also provides a way to attach a hidden separated DOM to an element

## What are the required files for a Lightning Web Component?

    JavaScript file - Holds our component's Javascript class, used to control the HTML template.
    HTML file - Contains the html template that will render our components view
    Component Meta Data file - 	defines the metadata values for the component, including supported targets and the design configuration for Lightning App Builder and Experience Builder.
    Optional CSS file    

## Are there any naming requirements to this bundle?

    Begins with lowercase letter
    Only alphanumeric characters or underscores
    Must be unique within the namespace
    No whitespace
    Can't end in an underscore
    Can't have 2 consecutive underscores
    Can't have hyphens
    Must have the same name as the folder

## How can we declare another custom component within our HTML?

    <namespace-component-name></namespace-component-name>

## How do you achieve data binding in LWC?

    Use {jsVariable/jsGetter} In the component's HTML template

## How do you achieve action binding in LWC?

    Use { jsClassFunction } on an element's browser event(on click, onsubmit, etc..)

## What are the Directives in LWC?

    Directives are a special attribute that adds dynamic behavior to an HTML template. Excluding key directives, they must be used on a nested template tag. 
    We have for:each, for:item, for:index, if:true, if:false, iterator:iteratorName, and key directives.

## What is the component Lifecycle?

    Component's constructor is called
    Public Properties are updated if needed
    Component is inserted into the DOM(connectedCallback() lifecycle hook is called)
    Component is rendered
    Child Component constructor is called
    Child public properies are updated if needed
    Child component is inserted into the DOM(connectedCallback() lifecycle hook is called)
    Child component is rendered (renderedCallback() lifecycle hook called on the child, then it is called on the parent)
    Parent is removed from the DOM (disconnectedCallback() lifecycle hook is called on the parent)
    Child removed from the DOM (disconnectedCallback() lifecycle hook is called on the Child)

## What are the Lifecycle hooks?

    Components constructor() -  called to initialize the component.
    connectedCallback() - fires when the component is inserted in the DOM, use to preform initialization tasks such as fetch data, listen for events, subscribe to message channels, and more.
    disconnectedCallback() - fires when the component is revomed from the DOM, use to preform componmment clean-up
    renderCallback() - fires after the component is rendered. used for taking actions  after the view is rendered.
    errorCallback() - It captures errors that occur in the descendant's lifecycle hooks or during an event handler declared in an HTML template. Used to log errors and define error boundaries.
    
## What are The Decorators in LWC?

    Akin to annotations in Apex, Decorators are declaritive way to add functionality(meta data) to fields and functions on our Javascript class.
    There are 3 Decorators of importance in LWC:
	@api - exposes a field as a public property
	@track - if a field marked by @track value changes and it is used in the HTML template, the component will renders and displays the new value. Use only for collections and objects
	@wire -  Used to provision and read Salesforce org data into a field or function.

## What are public properties?

    When a field is marked with @api, it is exposed as a public field. A public field is a property of the component at is accessable to the parent component or the Lighttning App builder. 

## What is reactivity

    It is the ability of the LWC engine to observe values and their changes, which cause the component to rerender and display the new value. Fields that are used in the Html template, has a getter, or is marked with the @track are determined as reactive. The @track is needed with arrays and objects.

# LWC with Data

## What are the prebuilt components I can use to intergrate with the Lightning Data Service?

    lighting-record-form, lightning-record-edit-form, Lightning-record-view-form, 

## What is the wire service?

    The wire service provisions an immutable stream of data to the component using it, It can be used in combination with the Lightning Data Service to integrate with Salesforce Org Data

## How do I use the @wire annotation?

    use the annotation above a field or a function that we want the wire service to pipe data into, supply the annotation  with an LDS wire adapter such as createRecord, deleteRecord, getRecord, etc... then provide the adapterConfiguation(in the form of an object literal) that takes in a recordId and the fields you want to retrieve from the record. 

## What module can we use to import object and field API names from?

    import ObjectName from '@salesforce/schema/object'; for objectApi names
    import FieldName from '@salesforce/schema/objectName.fieldName';
    NOTE: we cannot import Person Accounts directly or External Objects

## How can I call Apex Methods with Wiring?

    First, the apex method needs to be static, global or public, and annotated with the @AuraEnabled(cacheable=true)
    Second, import the method into the Javascript class like the following example: import methodName from '@salesforce/apex/Namespace.Classname.apex.MethodReference';
    Third, provide the imported method into the first attribute of the @wire annotation, followed by an object literal mapping any method parameters. This is placed above a field or a function that takes the following object literal as the method parameter: {error, data}

##  How can I call an Apex Method Imperatively?

    Import the method into the JS class, then invoke the method in a JS function and handle the promise returned with .then and .catch functions.

## How can I create a and dispatch a custom event in LWC?

    Using the CustomEvent interface, you can create a custom event with new CustomEvent('eventName', { //object literal for configuration of the event such as setting details carried with the event and the propagation type }), then use the this.dispatchEvent() function to fire it.

## What is the Lightning Messaging Service?

    A way to communicate between two components, related or un-relateded components in a single page or across multiple pages over a messaging channel.
    You have a component publish a message to the channel, and then another component subscribe to the channel and handle the promised returned.
    MessageChannels can be subscribed t

## What are the steps to creating a Message Channel? 

    Steps to create a channel:
        Create a folder in the project called messageChannels
        Create a file in that folder called messageChannelName.messageChannel-meta.xml
        Supply the proper xml tags
	Deploy to org
		bonus if the can get the xml tags in the file:
			```
<LightningMessageChannel xmlns="http://soap.sforce.com/2006/04/metadata">
    <masterLabel>RecordSelectedMessageChannel</masterLabel>
    <isExposed>true</isExposed>
    <description>This is a sample Lightning Message Channel.</description>
    <lightningMessageFields>
        <fieldName>message</fieldName>
        <description>This is sample message</description>
    </lightningMessageFields>
</LightningMessageChannel>

			```

## How do we create test files to be ran by Jest?

    Create a __tests__ folder and, as a best practice, use files that end in .test.js
    Or you can use the CLI command sfdx force:lightning:lwc:test:create -f path/to-component-javascript-file.js

## What is the structure of a Jest test

    describe block - Defines a test suite, contains one-or more tests.
    afterEach block - runs after every test, removes the element off the DOM for a clean next test.
    it block - Defines a unit test(a test case), first it creates and attaches to the component being tested, then we use comparison methods expect(acutalValueFromHtml).toBe(expectedValue)

## What are Lightning Web Components Open Source?

    Allows us to use LWC on any platform, not just salesforce. This makes creating third party apps to integrate with salesforce with the same framework.
    You can use Lightning Out to have our open source component access Salesforce data

# WHITEBOARDING QUESTIONS

## Create a LWC that takes a number from a user and displays that number multiplied by 3 somewhere else on the page.

```
<!-- numberChange.html -->
<template>
    <p>Your number x 3 is: {myNum}!</p>
    <lightning-input label="Number" value={myNum} onchange={handleChange}></lightning-input>
</template>

// numberChange.js
import { LightningElement } from 'lwc';

export default class NumberChange extends LightningElement {
    myNum = 0;

    handleChange(event) {
        this.myNum = event.target.value * 3;
    }
}
```

## Create a LWC that fires an event called "CustomClick" when a button is clicked.
```
<!-- HTML -->
<template>
	<lightning-layout>
		<lightning-layout-item>
			<lightning-button label="Click Me" onclick={clickHandler}></lightning-button>
		</lightning-layout-item>
	</lightning-layout>
</template>

// JS
import { LightningElement } from 'lwc';

export default class EventSource extends LightningElement {
	clickHandler() {
		this.dispatchEvent(new CustomEvent('customclick'));
	}
}
```

## Call an Apex Method called myMethod imparatively

```
 import myMethod from'@slaesforce/apex/apexClass.myMethod';

export default class EventSource extends LightningElement {

	data;
        error

	handleLoad(){
	    myMethod().then(result => {
                this.data = result;
            }).catch(error => {
                this.error = error;
            });

	}
}

```

## Write the html markup to iterate through a list of accounts stored in the variable accountList, and display the account name in a p tag

```
<template>
 <template for:each={accountList} for:item="account">
            <p key={account.id}>{account.Name}</p>
 </template>
</tempalte>

```
